package b.tfr;

/**
 * Main.java
 *
 * Parse the command line arguments and then run the recovery appropriately.
 **/
public class Main{
  private JSON feedItems;
  private String output;
  private String type;

  /**
   * main()
   *
   * Run an instance of the Main object, creating an object context.
   *
   * @param args The command line arguments.
   **/
  public static void main(String[] args){
    new Main(args);
  }

  /**
   * Main()
   *
   * Parse the command line arguments and execute the program appropriately.
   *
   * @param args The command line arguments.
   **/
  public Main(String[] args){
    /* Initialize variables */
    feedItems = null;
    output = null;
    type = "rss";
    /* Make sure we were given arguments */
    if(args.length <= 0){
      System.err.println("No arguments given");
      System.exit(0);
    }
    /* Loop over command line arguments */
    for(int x = 0; x < args.length; x++){
      /* Find which command line arguments we have */
      switch(args[x]){
        case "-h" :
        case "--help" :
          x = help(args, x);
          break;
        case "-i" :
        case "--feed-items" :
          x = feedItems(args, x);
          break;
        case "-o" :
        case "--output" :
          x = output(args, x);
          break;
        case "-t" :
        case "--type" :
          x = type(args, x);
          break;
        default :
          System.err.println("Unknown argument '" + args[x] + "'");
          break;
      }
    }
    /* Attempt to run the program */
    if(feedItems != null){
      /* Initializer a feed items parser */
      ParseFeedItems pfi = new ParseFeedItems(feedItems, type);
      /* Ensure there were no issues on initialization */
      if(pfi.validType()){
        pfi.parse();
      }else{
        System.err.println("Incorrect type given '" + type + "'");
      }
      /* Ensure there were no issues on parse */
      if(pfi.validParse()){
        pfi.save(output);
      }else{
        System.err.println("Was unable to parse the input file");
      }
    }else{
      System.err.println("Not enough input data given");
    }
  }

  /**
   * help()
   *
   * Print the help and quit the program execution.
   *
   * @param args The command line arguments.
   * @param x The current command line offset.
   * @return The new command line offset.
   **/
  private int help(String[] args, int x){
    System.err.println("java -jar dist/tfr.jar <OPT>");
    System.err.println("");
    System.err.println("  OPTions");
    System.err.println("");
    System.err.println("    -h  --help        Display this help");
    System.err.println("    -i  --feed-items  Provide feed items file");
    System.err.println("                        <FILE> The JSON file");
    System.err.println("    -o  --output      Output file");
    System.err.println("                        <FILE> The output filename");
    System.err.println("    -t  --type        Output file type");
    System.err.println("                        <STR>  The type of output file");
    System.err.println("                          opml  RSS OPML file");
    System.err.println("                          rss   Basic list of RSS feed URLs");
    System.exit(0);
    return x;
  }

  /**
   * feedItems()
   *
   * Get the feed items from the command line.
   *
   * @param args The command line arguments.
   * @param x The current command line offset.
   * @return The new command line offset.
   **/
  private int feedItems(String[] args, int x){
    if(++x >= args.length){
      System.err.println("Unable to read feeditems");
    }else{
      try{
        feedItems = JSON.build(args[x]);
      }catch(Exception e){
        System.err.println("Unable to parse feedtimes to JSON");
      }
    }
    return x;
  }

  /**
   * output()
   *
   * Set the output filename.
   *
   * @param args The command line arguments.
   * @param x The current command line offset.
   * @return The new command line offset.
   **/
  private int output(String[] args, int x){
    if(++x >= args.length){
      System.err.println("Unable to set output");
    }else{
      output = args[x];
    }
    return x;
  }

  /**
   * type()
   *
   * Set the output type.
   *
   * @param args The command line arguments.
   * @param x The current command line offset.
   * @return The new command line offset.
   **/
  private int type(String[] args, int x){
    if(++x >= args.length){
      System.err.println("Unable to set type");
    }else{
      type = args[x];
    }
    return x;
  }
}
