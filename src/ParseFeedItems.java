package b.tfr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

/**
 * ParseFeedItems.java
 *
 * A class dedicated to parsing feed items.
 **/
public class ParseFeedItems{
  private JSON fi;
  private String type;
  private boolean parse;
  private HashMap<String, JSON> feeds;

  /**
   * ParseFeedItems()
   *
   * Initialize the feed items for parsing.
   *
   * @param fi The feed items JSON file.
   * @param type The type that will be converted to.
   **/
  public ParseFeedItems(JSON fi, String type){
    /* Store variables */
    this.fi = fi;
    this.type = type;
    /* Initialize internal variables */
    parse = false;
    feeds = new HashMap<String, JSON>();
  }

  /**
   * validType()
   *
   * Check whether the type is valid for the given input file.
   *
   * @return True if the type is valid, otherwise false.
   **/
  public boolean validType(){
    /* Make sure the input JSON exists */
    if(fi == null){
      return false;
    }
    /* Make sure we were given a type */
    if(type == null){
      return false;
    }
    /* Check the output type */
    switch(type){
      case "opml" :
        return true;
      case "rss" :
        return true;
      default :
        return false;
    }
  }

  /**
   * parse()
   *
   * Perform the parse process.
   **/
  public void parse(){
    parse = true;
    /* Loop over each items */
    for(int x = 0; x < fi.length(); x++){
      /* Get the feeds */
      JSON urls = fi.get(x).get("feedURLs");
      /* Process each feed */
      for(int i = 0; i < urls.length(); i++){
        String key =
          urls.get(i).value("none")
            .replaceAll("&", "&amp;")
            .replaceAll("<", "&lt;")
            .replaceAll(">", "&gt;");
        /* If the feed doesn't exist, add it */
        if(feeds.get(key) == null){
          try{
            feeds.put(key, new JSON("{}"));
            feeds.get(key).set(new JSON("rss-url", key));
            feeds.get(key).set(new JSON("\"items\":[]"));
          }catch(Exception e){
            System.err.println("Failed to process JSON");
            e.printStackTrace();
            parse = false;
          }
        }
        /* Add the new item */
        try{
          feeds.get(key).get("items").set(
            new JSON("\"" +
              fi.get(x).key("none")
                .replaceAll("&", "&amp;")
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;")
                .replaceAll("\\P{Print}", "") +
            "\"")
          );
        }catch(Exception e){
          System.err.println("Failed to add item");
          e.printStackTrace();
        }
      }
    }
    /* Display statistics */
    System.err.println("Found " + feeds.size() + " unique feeds");
  }

  /**
   * validParse()
   *
   * Check whether the parse process was completed successfully.
   *
   * @return True if completed without issue, otherwise false.
   **/
  public boolean validParse(){
    return parse;
  }

  public void save(String output){
    /* TODO: Select between stdout and a file. */
    OutputStream os = System.out;
    if(output != null){
      try{
        os = new FileOutputStream(new File(output), true);
      }catch(IOException e){
        System.err.println("Failed to open output file '" + output + "'");
        e.printStackTrace();
      }
    }
    StringBuilder sb = new StringBuilder();
    /* Build the initial file header */
    switch(type){
      case "opml" :
        sb
          .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
          .append("<opml version=\"1.0\">\n")
          .append("  <head>\n")
//          .append("    <text/>\n")
          .append("  </head>\n")
          .append("  <body>\n")
          .append("    <outline")
            .append(" text=\"all\"")
            .append(" title=\"all\"")
//            .append(" isOpen=\"false\"")
//            .append(" id=\"TODO\"")
            .append(">\n");
        break;
    }
    /* Output the required type in feed loop */
    for(String s : feeds.keySet()){
      JSON feed = feeds.get(s);
      switch(type){
        case "opml" :
          sb
            .append("      <outline")
              .append(" title=\"" + feed.get("items").get(0).value("none") + "\"")
              .append(" text=\"" + feed.get("items").get(0).value("none") + "\"")
              .append(" htmlUrl=\"" + feed.get("rss-url").value("none") + "\"")
//              .append(" comment=\"" + "" + "\"")
              .append(" xmlUrl=\"" + feed.get("rss-url").value("none") + "\"")
//              .append(" id=\"" + "TODO" + "\"")
//              .append(" useCustomFetchInterval=\"" + "false" + "\"")
//              .append(" copyright=\"" + "" + "\"")
              .append(" version=\"" + "RSS" + "\"")
//              .append(" description=\"" + "TODO" + "\"")
              .append(" type=\"" + "rss" + "\"")
//              .append(" archiveMode=\"" + "globalDefault" + "\"")
//              .append(" faviconUrl=\"" + "TODO" + "\"")
//              .append(" fetchInterval=\"" + "0" + "\"")
//              .append(" maxArticleAge=\"" + "0" + "\"")
//              .append(" maxArticleNumber=\"" + "0" + "\"")
              .append("/>\n");
          break;
        case "rss" :
          sb
            .append(feed.get("rss-url").value("none"))
            .append("\n");
          break;
      }
    }
    /* Build the file footer */
    switch(type){
      case "opml" :
        sb
          .append("    </outline>\n")
          .append("  </body>\n")
          .append("</opml>\n\n");
        break;
    }
    try{
      os.write(sb.toString().getBytes());
      os.flush();
      os.close();
    }catch(IOException e){
      System.err.println("Failed to write to output");
      e.printStackTrace();
    }
  }
}
