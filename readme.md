# Thunderbird Feed Recovery

This is a solution to the problem where in your
`~/.thunderbird/<ID>.default/Mail/Feeds/` directory you can find a valid
`feeditems.json`, but not a valid `feeds.json` file. This tool is intended to
help recover your RSS feeds from a corrupted file.

## Build

To build you will need a valid version of `java` and the `ant` build system.
You can then run:

    ant

## Run

To run the program, it is a case of running (if you have properly built the
program):

    java -jar dist/tfr.jar
